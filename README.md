# Contao Erweiterung 'Locations'
Diese Erweiterung ermöglicht das einfache zuweisen von wiederkehrenden Locations zu Ihren Events. Des Weiteren können Sie die Veranstaltungsorte mit sehr viel mehr Informationen bestücken und in allen event_ Templates ausgeben.
Beispiele:
Name der Location: $this->location->title 
Firma der Location: $this->location->company 
Telefonnummer: $this->location->phone

Wichtig: Nehmen Sie vor der Installation eine Sicherung der Datenbanktabelle tl_calendar_events vor!
Die Contao Erweiterung prüft nach der Installation, ob es in bisherigen Events Veranstaltungsorte gibt. Sollte das der Fall sein, werden die entsprechenden Veranstaltungsorte automatisch erstellt und dem jeweiligen Event zugewiesen.

Vorsicht in bestehenden Installationen: Wenn Sie diese Erweiterung wieder deinstallieren sollten, verlieren Sie die Informationen über die Veranstaltungsorte des jeweiligen Events. Wir arbeiten in einer der nächsten Versionen daran, eine problemfreie Deinstallation zu ermöglichen.

Folgende Felder können ausgegeben werden: Firma, Ansprechpartner, Straße, PLZ, Stadt, Bundesland, Land, Telefonnummer, Mobilnummer, Faxnummer, E-Mail-Adresse, Website sowie ein Bild.

## Weitere Informationen und Support verfügbar unter:
https://www.fast-media.net/de/produkte/contao-erweiterung/wiederkehrende-orte/

# Kompatibilität
Die Version 4.5.0 der Erweiterung ist kompatibel mit Contao 4.4.0 bis 4.9.x.
Vermutlich funktioniert die Erweiterung auch mit Contao 4.13.

Definitiv getestet wurden folgende Versionen:

* Contao 4.4 LTS (diverse Installationen)
* Contao 4.9 LTS (diverse Installationen)

## Features
* Ausgabe diverser weiterer Informationen über den Veranstaltungsort je Event

## Kurzerklärung
Die Erweiterung bringt erweiterte Backend-Funktionalitäten mit sich. Nach der Installation der Erweiterung im Contao Manager, ändern sich zwei Dinge im Contao Backend:

* Es erscheint ein neuer Button in der Liste der Kalender (Events)
* Es erscheint ein neuer Button innerhalb des jeweiligen Kalenders
* Es erscheint ein neuer Button innerhalb eines Newsarchivs
