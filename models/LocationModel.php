<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Locations;


/**
 * Reads and writes locations
 */
class LocationModel extends \Model
{

	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_location';

	/**
	 * Find published news items by their parent ID
	 *
	 * @param array   $arrPids     An array of news archive IDs
	 * @param boolean $blnFeatured If true, return only featured news, if false, return only unfeatured news
	 * @param integer $intLimit    An optional limit
	 * @param integer $intOffset   An optional offset
	 * @param array   $arrOptions  An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findPublishedByPids($arrPids=array(), $intLimit=0, $intOffset=0, array $arrOptions=array())
	{
		$t = static::$strTable;

		if (is_array($arrPids) && !empty($arrPids))
		{
			$arrColumns = array("$t.pid IN(" . implode(',', array_map('intval', $arrPids)) . ")");
		}

		if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
		{
			$arrColumns[] = "$t.published=1";
		}

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order']  = "$t.title ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;

		return static::findBy($arrColumns, null, $arrOptions);
	}


	/**
	 * Count published contact items by their parent ID
	 *
	 * @param array   $arrPids     An array of contact group IDs
	 * @param array   $arrOptions  An optional options array
	 *
	 * @return integer The number of contact items
	 */
	public static function countPublishedByPids($arrPids, array $arrOptions=array())
	{
		$t = static::$strTable;

		if (is_array($arrPids) && !empty($arrPids))
		{
			$arrColumns = array("$t.pid IN(" . implode(',', array_map('intval', $arrPids)) . ")");
		}

		if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
		{
			$arrColumns[] = "$t.published=1";
		}

		//Try to find by search
		if (\Input::get('for'))
		{
			$for = \Input::get('for');
			$arrColumns[] = "($t.email LIKE '%".$for."%' OR $t.firstname LIKE '".$for."%' OR $t.lastname LIKE '".$for."%')";
		}

		return static::countBy($arrColumns, null, $arrOptions);
	}


	/**
	 * Find published news items by their parent ID
	 *
	 * @param array   $arrPids     An array of news archive IDs
	 * @param boolean $blnFeatured If true, return only featured news, if false, return only unfeatured news
	 * @param integer $intLimit    An optional limit
	 * @param integer $intOffset   An optional offset
	 * @param array   $arrOptions  An optional options array
	 *
	 * @return \Model\Collection|null A collection of models or null if there are no news
	 */
	public static function findPublishedByIds($arrIds=array(), $intLimit=0, $intOffset=0, array $arrOptions=array())
	{
		$t = static::$strTable;
		if($arrIds) {
			$arrColumns = array("($t.id = ".implode(' OR '.$t.'.id = ',array_map('intval', $arrIds)).")");
		}

		if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
		{
			$arrColumns[] = "$t.published=1";
		}

		if (!isset($arrOptions['order']))
		{
			$arrOptions['order']  = "$t.title ASC";
		}

		$arrOptions['limit']  = $intLimit;
		$arrOptions['offset'] = $intOffset;

		return static::findBy($arrColumns, null, $arrOptions);
	}

	/**
	 * Find locations by ID or alias
	 *
	 * @param mixed $varId The numeric ID or alias name
	 *
	 * @return \Model|null The ProductCategoryModel or null if there is no category
	 */
	public static function findPublishedByIdOrAlias($varId)
	{
		$t = static::$strTable;
		$arrColumns = array("($t.id=? OR $t.alias=?)");

		if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
		{
			$arrColumns[] = "$t.published=1";
		}

		return static::findBy($arrColumns, array((is_numeric($varId) ? $varId : 0), $varId));
	}

	/**
	 * Find locations by city
	 *
	 * @param mixed $varId The numeric ID or alias name
	 *
	 * @return \Model|null The ProductCategoryModel or null if there is no category
	 */
	public static function findPublishedByCities($arrIds=array())
	{
		$t = static::$strTable;

		$arrColumns = array();

		$arrColumns[] = "$t.city!=''";

		if($arrIds)
		{
			$arrColumns = "($t.city = '".implode('\' OR '.$t.'.city = \'', $arrIds)."')";
		}

		if (!BE_USER_LOGGED_IN || TL_MODE == 'BE')
		{
			$arrColumns[] = "$t.published=1";
		}

		$arrOptions['group'] = 'city';

		return static::findBy($arrColumns, null, $arrOptions);
	}
}
