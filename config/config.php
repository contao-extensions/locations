<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Back end modules
 */
 // Check if extension 'calendar' is installed
if ($GLOBALS['BE_MOD']['content']['calendar'])
{
	$GLOBALS['BE_MOD']['content']['calendar']['tables'][] = 'tl_location_archive';
	$GLOBALS['BE_MOD']['content']['calendar']['tables'][] = 'tl_location';

	/**
	 * Hooks
	 */
	if (TL_MODE == 'FE')
	{
		$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('Locations\ModuleEventChange', 'modifyTemplate');
		$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('Locations\ModuleNewsChange', 'modifyTemplate');
	}
}
else
{
	$GLOBALS['BE_MOD']['content']['location'] = array
	(
		'tables'						=> array('tl_location_archive')
	);
}

/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'locations';
$GLOBALS['TL_PERMISSIONS'][] = 'locationp';
