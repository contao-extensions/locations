<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'Locations',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Models
	'Locations\LocationArchiveModel' => 'system/modules/locations/models/LocationArchiveModel.php',
	'Locations\LocationModel'        => 'system/modules/locations/models/LocationModel.php',

	// Modules
	'Locations\ModuleEventChange'    => 'system/modules/locations/modules/ModuleEventChange.php',
	'Locations\ModuleNewsChange'    => 'system/modules/locations/modules/ModuleNewsChange.php'
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'event_full' => 'system/modules/locations/templates/events',
	'event_list' => 'system/modules/locations/templates/events',
	'event_teaser' => 'system/modules/locations/templates/events'
));
