<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


class LocationsRunonceJob extends Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->import('Database');
	}


	public function run()
	{
		// Search for locations
		if(\Database::getInstance()->tableExists('tl_calendar_events') && !\Database::getInstance()->tableExists('tl_location_archive'))
		{
			$objEvents = \Database::getInstance()->execute("SELECT id, location FROM tl_calendar_events WHERE location != '' AND location != '0'");

			if($objEvents->numRows > 0)
			{

				// Create location archive table
				\Database::getInstance()->execute("CREATE TABLE `tl_location_archive` (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`tstamp` int(10) unsigned NOT NULL DEFAULT '0',
					`title` varchar(255) NOT NULL DEFAULT '',
					PRIMARY KEY (`id`)
				)");

				// Insert first archive
				$arrSet = array
				(
					'tstamp' => time(),
					'title' => 'Veranstaltungsorte'
				);

				$pid = \Database::getInstance()->prepare("INSERT INTO tl_location_archive %s")->set($arrSet)->execute()->insertId;

				// Create location table
				\Database::getInstance()->execute("CREATE TABLE `tl_location` (
					`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
					`pid` int(10) unsigned NOT NULL DEFAULT '0',
					`tstamp` int(10) unsigned NOT NULL DEFAULT '0',
					`title` varchar(255) NOT NULL DEFAULT '',
					`published` char(1) NOT NULL DEFAULT '',
					PRIMARY KEY (`id`)
				)");

				while($objEvents->next())
				{
					// Search for same location
					$intLocationId = \Database::getInstance()->execute("SELECT id FROM tl_location WHERE title = '$objEvents->location'")->id;

					if(!$intLocationId)
					{
						// Insert new location
						$arrSet = array
						(
							'pid' => $pid,
							'tstamp' => time(),
							'title' => $objEvents->location,
							'published' => 1
						);

						$intLocationId = \Database::getInstance()->prepare("INSERT INTO tl_location %s")->set($arrSet)->execute()->insertId;
					}

					// Update Event
					$arrSet = array
					(
						'location' => $intLocationId,
					);

					\Database::getInstance()->prepare("UPDATE tl_calendar_events %s WHERE id = ?")->set($arrSet)->execute($objEvents->id);
				}
			}

			// Update every event which has no location
			\Database::getInstance()->prepare("UPDATE tl_calendar_events SET location=? WHERE location=?")->execute(0, '');
		}

	}
}

$objRunonceJob = new LocationsRunonceJob();
$objRunonceJob->run();