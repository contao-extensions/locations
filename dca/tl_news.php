<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

// Check if extension 'news' is installed
if (in_array('news', \ModuleLoader::getActive()))
{
	$strTable = 'tl_news';

	/**
	 * Add a global operation to tl_calendar_events
	 */
	array_insert($GLOBALS['TL_DCA'][$strTable]['list']['global_operations'], 1, array
	(
		'locations' => array
		(
			'href'							=> 'table=tl_location_archive',
			'class'							=> 'header_locations',
			'icon'							=> 'system/modules/locations/assets/location.png',
			'attributes'				=> 'onclick="Backend.getScrollOffset()" accesskey="c"'
		)
	));



	/**
	 * Palettes
	 */
	if($GLOBALS['TL_DCA'][$strTable]['palettes']) {
		foreach($GLOBALS['TL_DCA'][$strTable]['palettes'] as $name => $palette )
		{

			$GLOBALS['TL_DCA'][$strTable]['palettes'][$name] = str_replace(
				array(
					';{publish_legend'
				),
				array(
					',location;{publish_legend',
					),
				$palette
			);
		}
	}


	/**
	 * Fields
	 */
	$GLOBALS['TL_DCA'][$strTable]['fields']['location'] = array
	(
		'exclude'						=> true,
		'search'						=> true,
		'filter'						=> true,
		'sorting'						=> true,
		'flag'							=> 1,
		'inputType'					=> 'select',
		'foreignKey'				=> 'tl_location.title',
		'options_callback'	=> array('tl_news_locations', 'getLocations'),
		'wizard'						=> array(array('tl_news_locations', 'editLocations')),
		'eval'							=> array('chosen'=>true, 'includeBlankOption'=>true, 'tl_class'=>'w50'),
		'sql'								=> "int(10) unsigned NOT NULL default '0'"
	);


	/**
	 * Provide miscellaneous methods that are used by the data configuration array.
	 */
	class tl_news_locations extends Backend
	{

		/**
		 * Return the delete archive button
		 * @param array
		 * @param string
		 * @param string
		 * @param string
		 * @param string
		 * @param string
		 * @return string
		 */
		public function getLocations()
		{

			$arrItems = array();
			$objItems = \Database::getInstance()->execute("SELECT id, title, city AS category FROM tl_location ORDER BY city ASC, title ASC");

			while ($objItems->next())
			{
				// Verhindern, dass Filter auf Übersichtsseite nicht mehr funktioniert
				if(\Input::get('act')) { $arrItems[$objItems->category][$objItems->id] = $objItems->title; }
				else { $arrItems[$objItems->id] = $objItems->title; }
			}

			return $arrItems;
		}


		/**
		 * Return the edit module wizard
		 * @param \DataContainer
		 * @return string
		 */
		public function editLocations(DataContainer $dc)
		{
			return ($dc->value < 1) ? '' : ' <a href="contao?do=calendar&amp;table=tl_location&amp;act=edit&amp;id=' . $dc->value . '&amp;popup=1&amp;nb=1&amp;rt=' . REQUEST_TOKEN . '" title="' . sprintf(specialchars($GLOBALS['TL_LANG']['tl_content']['editalias'][1]), $dc->value) . '" style="padding-left:3px" onclick="Backend.openModalIframe({\'width\':768,\'title\':\'' . specialchars(str_replace("'", "\\'", sprintf($GLOBALS['TL_LANG']['tl_content']['editalias'][1], $dc->value))) . '\',\'url\':this.href});return false">' . Image::getHtml('alias.svg', $GLOBALS['TL_LANG']['tl_content']['editalias'][0], 'style="vertical-align:top"') . '</a>';
		}
	}
}