<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location_archive';

/**
 * Table tl_location_archive
 */
$GLOBALS['TL_DCA'][$strTable] = array
(

	// Config
	'config' => array
	(
		'dataContainer'				=> 'Table',
		'ctable'							=> array('tl_location'),
		'switchToEdit'				=> true,
		'enableVersioning'		=> true,
		'onload_callback' 		=> array
		(
			array($strTable, 'checkPermission')
		),
		'sql'									=> array
		(
			'keys' => array
			(
				'id' => 'primary'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'							=> 1,
			'fields'						=> array('title'),
			'flag'							=> 1,
			'panelLayout'				=> 'search,limit'
		),
		'label' => array
		(
			'fields'						=> array('title'),
			'format'						=> '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'href'						=> 'act=select',
				'class'						=> 'header_edit_all',
				'attributes'			=> 'onclick="Backend.getScrollOffset();"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'href'						=> 'table=tl_location',
				'icon'						=> 'edit.svg'
			),
			'editheader' => array
			(
				'href'						=> 'act=edit',
				'icon'						=> 'header.svg',
				'button_callback'	=> array($strTable, 'editHeader')
			),
			'copy' => array
			(
				'href'						=> 'act=copy',
				'icon'						=> 'copy.svg',
				'button_callback'	=> array($strTable, 'copyArchive')
			),
			'delete' => array
			(
				'href'						=> 'act=delete',
				'icon'						=> 'delete.svg',
				'attributes'			=> 'onclick="if (!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\')) return false; Backend.getScrollOffset();"',
				'button_callback'	=> array($strTable, 'deleteArchive')
			),
			'show' => array
			(
				'href'						=> 'act=show',
				'icon'						=> 'show.svg'
			),
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'				=> array(),
		'default'							=> '{title_legend},title,alias,jumpTo;{protected_legend:hide},protected',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'								=> "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'								=> "int(10) unsigned NOT NULL default '0'"
		),
		'title' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('mandatory'=>true, 'maxlength'=>255),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'jumpTo' => array
		(
			'exclude'						=> true,
			'inputType'					=> 'pageTree',
			'foreignKey'				=> 'tl_page.title',
			'eval'							=> array('fieldType'=>'radio'),
			'sql'								=> "int(10) unsigned NOT NULL default '0'",
			'relation'					=> array('type'=>'hasOne', 'load'=>'eager')
		)
	)
);


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 */
class tl_location_archive extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Check permissions to edit table tl_location_archive
	 */
	public function checkPermission()
	{

		if ($this->User->isAdmin)
		{
			return;
		}

		// Set root IDs
		if (!is_array($this->User->locations) || empty($this->User->locations))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->locations;
		}

		$GLOBALS['TL_DCA']['tl_location_archive']['list']['sorting']['root'] = $root;

		// Check permissions to add groups
		if (!$this->User->hasAccess('create', 'locationp'))
		{
			$GLOBALS['TL_DCA']['tl_location_archive']['config']['closed'] = true;
		}

		// Check current action
		switch (Input::get('act'))
		{
			case 'create':
			case 'select':
				// Allow
				break;

			case 'edit':
				// Dynamically add the record to the user profile
				if (!in_array(Input::get('id'), $root))
				{
					$arrNew = $this->Session->get('new_records');

					if (is_array($arrNew['tl_location_archive']) && in_array(Input::get('id'), $arrNew['tl_location_archive']))
					{
						// Add permissions on user level
						if ($this->User->inherit == 'custom' || !$this->User->groups[0])
						{
							$objUser = \Database::getInstance()->prepare("SELECT locations, locationp FROM tl_user WHERE id=?")
												->limit(1)
												->execute($this->User->id);

							$arrLocationp = deserialize($objUser->locationp);

							if (is_array($arrLocationp) && in_array('create', $arrLocationp))
							{
								$arrLocations = deserialize($objUser->locations);
								$arrLocations[] = Input::get('id');

								\Database::getInstance()->prepare("UPDATE tl_user SET locations=? WHERE id=?")
										->execute(serialize($arrLocations), $this->User->id);
							}
						}

						// Add permissions on group level
						elseif ($this->User->groups[0] > 0)
						{
							$objGroup = \Database::getInstance()->prepare("SELECT locations, locationp FROM tl_user_group WHERE id=?")
												->limit(1)
												->execute($this->User->groups[0]);

							$arrLocationp = deserialize($objGroup->locationp);

							if (is_array($arrLocationp) && in_array('create', $arrLocationp))
							{
								$arrLocations = deserialize($objGroup->locations);
								$arrLocations[] = Input::get('id');

								\Database::getInstance()->prepare("UPDATE tl_user_group SET locations=? WHERE id=?")
									->execute(serialize($arrLocations), $this->User->groups[0]);
							}
						}

						// Add new element to the user object
						$root[] = Input::get('id');
						$this->User->locations = $root;
					}
				}
				// No break;

			case 'copy':
			case 'delete':
			case 'show':
				if (!in_array(Input::get('id'), $root) || (Input::get('act') == 'delete' && !$this->User->hasAccess('delete', 'locationp')))
				{
					$this->log('Not enough permissions to '.Input::get('act').' locations group ID "'.Input::get('id').'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				break;

			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
				$session = $this->Session->getData();
				if (Input::get('act') == 'deleteAll' && !$this->User->hasAccess('delete', 'locationp'))
				{
					$session['CURRENT']['IDS'] = array();
				}
				else
				{
					$session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $root);
				}
				$this->Session->setData($session);
				break;

			default:
				if (strlen(Input::get('act')))
				{
					$this->log('Not enough permissions to '.Input::get('act').' locations groups', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				break;
		}
	}


	/**
	 * Return the edit header button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function editHeader($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->canEditFieldsOf('tl_locations_group') ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
	}


	/**
	 * Return the copy group button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function copyArchive($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->hasAccess('create', 'locationp') ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
	}


	/**
	 * Return the delete group button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function deleteArchive($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->hasAccess('delete', 'locationp') ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ' : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
	}
}