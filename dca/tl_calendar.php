<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_calendar';

/**
 * Add a global operation to tl_calendar_events
 */
array_insert($GLOBALS['TL_DCA'][$strTable]['list']['global_operations'], 1, array
(
	'locations' => array
	(
		'href'							=> 'table=tl_location_archive',
		'class'							=> 'header_locations',
		'icon'							=> 'system/modules/locations/assets/location.png',
		'attributes'				=> 'onclick="Backend.getScrollOffset()" accesskey="c"'
	)
));