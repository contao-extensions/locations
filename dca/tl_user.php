<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_user';

/**
 * Add a palette to tl_user
 */
$GLOBALS['TL_DCA'][$strTable]['palettes']['custom'] = str_replace('{forms_legend}', '{location_legend},locations,locationp;{forms_legend}', $GLOBALS['TL_DCA'][$strTable]['palettes']['custom']);
$GLOBALS['TL_DCA'][$strTable]['palettes']['extend'] = str_replace('{forms_legend}', '{location_legend},locations,locationp;{forms_legend}', $GLOBALS['TL_DCA'][$strTable]['palettes']['extend']);

$GLOBALS['TL_DCA'][$strTable]['fields']['locations'] = array
(
	'exclude'								=> true,
	'inputType'							=> 'checkbox',
	'foreignKey'						=> 'tl_location_archive.title',
	'eval'									=> array('multiple'=>true),
	'sql'										=> "blob NULL"
);

$GLOBALS['TL_DCA'][$strTable]['fields']['locationp'] = array
(
	'exclude'								=> true,
	'inputType'							=> 'checkbox',
	'options'								=> array('create', 'delete'),
	'reference'							=> &$GLOBALS['TL_LANG']['MSC'],
	'eval'									=> array('multiple'=>true),
	'sql'										=> "blob NULL"
);
