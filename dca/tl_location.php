<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location';

/**
 * Load tl_content language file
 */
System::loadLanguageFile('tl_content');

/**
 * Table tl_location
 */
$GLOBALS['TL_DCA'][$strTable] = array
(

	// Config
	'config' => array
	(
		'dataContainer'				=> 'Table',
		'ptable'							=> 'tl_location_archive',
		'enableVersioning'		=> true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid' => 'index',
				'alias' => 'index',
				'published' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'							=> 4,
			'fields'						=> array('sorting', 'title'),
			'headerFields'			=> array('title', 'jumpTo', 'tstamp', 'protected', 'allowComments'),
			'panelLayout'				=> 'filter;sort,search,limit',
			'child_record_callback'	 => array($strTable, 'listObjects')
		),
		'label' => array
		(
			'fields'						=> array('title'),
			'format'						=> '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'href'						=> 'act=select',
				'class'						=> 'header_edit_all',
				'attributes'			=> 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'href'						=> 'act=edit',
				'icon'						=> 'edit.svg'
			),
			'copy' => array
			(
				'href'						=> 'act=paste&amp;mode=copy',
				'icon'						=> 'copy.svg'
			),
			'cut' => array
			(
				'href'						=> 'act=paste&amp;mode=cut',
				'icon'						=> 'cut.svg'
			),
			'delete' => array
			(
				'href'						=> 'act=delete',
				'icon'						=> 'delete.svg',
				'attributes'			=> 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'icon'						=> 'visible.svg',
				'attributes'			=> 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
				'button_callback'	=> array($strTable, 'toggleIcon')
			),
			'show' => array
			(
				'href'						=> 'act=show',
				'icon'						=> 'show.svg'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'				=> array('addImage', 'published'),
		'default'							=> '{title_legend},title,alias;{image_legend},addImage;{address_legend:hide},company,contact,street,postal,city,state,country;{contact_legend},phone,mobile,fax,email,website;{publish_legend},published'
	),

	// Subpalettes
	'subpalettes' => array
	(
		'addImage'						=> 'singleSRC,alt,size,imagemargin,imageUrl,fullsize,caption,floating'
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'								=> "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'				=> 'tl_location_archive.title',
			'sql'								=> "int(10) unsigned NOT NULL default '0'"
		),
		'sorting' => array
		(
			'sorting'						=> true,
			'flag'							=> 1,
			'sql'								=> "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sorting'						=> true,
			'flag'							=> 6,
			'sql'								=> "int(10) unsigned NOT NULL default '0'"
		),
		'title' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'sorting'						=> true,
			'flag'							=> 1,
			'inputType'					=> 'text',
			'eval'							=> array('mandatory'=>true, 'feEditable'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'alias' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('rgxp'=>'alias', 'unique'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
			'save_callback'			=> array
			(
				array($strTable, 'generateAlias')
			),
			'sql'								=> "varchar(128) COLLATE utf8_bin NOT NULL default ''"
		),
		'company' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'sorting'						=> true,
			'flag'							=> 1,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'contact' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'sorting'						=> true,
			'flag'							=> 1,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'street' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'postal' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'sorting'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>32, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(32) NOT NULL default ''"
		),
		'city' => array
		(
			'exclude'						=> true,
			'filter'						=> true,
			'search'						=> true,
			'sorting'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'state' => array
		(
			'exclude'						=> true,
			'sorting'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>64, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(64) NOT NULL default ''"
		),
		'country' => array
		(
			'exclude'						=> true,
			'filter'						=> true,
			'sorting'						=> true,
			'inputType'					=> 'select',
			'options'						=> System::getCountries(),
			'eval'							=> array('includeBlankOption'=>true, 'chosen'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'address', 'tl_class'=>'w50'),
			'sql'								=> "varchar(2) NOT NULL default ''"
		),
		'phone' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>64, 'rgxp'=>'phone', 'decodeEntities'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'								=> "varchar(64) NOT NULL default ''"
		),
		'mobile' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>64, 'rgxp'=>'phone', 'decodeEntities'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'								=> "varchar(64) NOT NULL default ''"
		),
		'fax' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>64, 'rgxp'=>'phone', 'decodeEntities'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'								=> "varchar(64) NOT NULL default ''"
		),
		'email' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'rgxp'=>'email', 'decodeEntities'=>true, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'website' => array
		(
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'feEditable'=>true, 'feViewable'=>true, 'feGroup'=>'contact', 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'addImage' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['addImage'],
			'exclude'						=> true,
			'filter'						=> true,
			'inputType'					=> 'checkbox',
			'eval'							=> array('submitOnChange'=>true, 'feViewable'=>true),
			'sql'								=> "char(1) NOT NULL default ''"
		),
		'singleSRC' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['singleSRC'],
			'exclude'						=> true,
			'inputType'					=> 'fileTree',
			'eval'							=> array('filesOnly'=>true, 'extensions'=>$GLOBALS['TL_CONFIG']['validImageTypes'], 'fieldType'=>'radio', 'feEditable'=>true),
			'sql'								=> "binary(16) NULL"
		),
		'alt' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['alt'],
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'tl_class'=>'long', 'feViewable'=>true),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'size' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['size'],
			'exclude'						=> true,
			'inputType'					=> 'imageSize',
			'options'						=> System::getImageSizes(),
			'reference'					=> &$GLOBALS['TL_LANG']['MSC'],
			'eval'							=> array('rgxp'=>'natural', 'includeBlankOption'=>true, 'nospace'=>true, 'helpwizard'=>true, 'tl_class'=>'w50'),
			'sql'								=> "varchar(64) NOT NULL default ''"
		),
		'imagemargin' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['imagemargin'],
			'exclude'						=> true,
			'inputType'					=> 'trbl',
			'options'						=> array('px', '%', 'em', 'ex', 'pt', 'pc', 'in', 'cm', 'mm'),
			'eval'							=> array('includeBlankOption'=>true, 'tl_class'=>'w50'),
			'sql'								=> "varchar(128) NOT NULL default ''"
		),
		'imageUrl' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['imageUrl'],
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('rgxp'=>'url', 'decodeEntities'=>true, 'maxlength'=>255, 'tl_class'=>'w50 wizard'),
			'wizard' => array
			(
				array($strTable, 'pagePicker')
			),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'fullsize' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['fullsize'],
			'exclude'						=> true,
			'inputType'					=> 'checkbox',
			'eval'							=> array('tl_class'=>'w50 m12'),
			'sql'								=> "char(1) NOT NULL default ''"
		),
		'caption' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['caption'],
			'exclude'						=> true,
			'search'						=> true,
			'inputType'					=> 'text',
			'eval'							=> array('maxlength'=>255, 'tl_class'=>'w50'),
			'sql'								=> "varchar(255) NOT NULL default ''"
		),
		'floating' => array
		(
			'label'							=> &$GLOBALS['TL_LANG']['tl_content']['floating'],
			'exclude'						=> true,
			'inputType'					=> 'radioTable',
			'options'						=> array('above', 'left', 'right', 'below'),
			'eval'							=> array('cols'=>4, 'tl_class'=>'w50'),
			'reference'					=> &$GLOBALS['TL_LANG']['MSC'],
			'sql'								=> "varchar(12) NOT NULL default ''"
		),
		'published' => array
		(
			'exclude'						=> true,
			'filter'						=> true,
			'flag'							=> 1,
			'inputType'					=> 'checkbox',
			'eval'							=> array('doNotCopy'=>true),
			'sql'								=> "char(1) NOT NULL default ''"
		)
	)
);

// Contao 4.4
if (version_compare(VERSION . '.' . BUILD, '4.5.0', '<'))
{
	$GLOBALS['TL_DCA'][$strTable]['fields']['alias']['sql'] = "varchar(128) COLLATE utf8_bin NOT NULL default ''";
}
else {
	$GLOBALS['TL_DCA'][$strTable]['fields']['alias']['sql'] = "varchar(128) BINARY NOT NULL default ''";
}


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 */
class tl_location extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}

	/**
	 * Check permissions to edit table tl_location
	 */
	public function checkPermission()
	{
		// HOOK: comments extension required
		if (!in_array('comments', ModuleLoader::getActive()))
		{
			$key = array_search('allowComments', $GLOBALS['TL_DCA']['tl_location']['list']['sorting']['headerFields']);
			unset($GLOBALS['TL_DCA']['tl_location']['list']['sorting']['headerFields'][$key]);
		}

		if ($this->User->isAdmin)
		{
			return;
		}

		// Set the root IDs
		if (!is_array($this->User->locations) || empty($this->User->locations))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->locations;
		}

		$id = strlen(Input::get('id')) ? Input::get('id') : CURRENT_ID;

		// Check current action
		switch (Input::get('act'))
		{
			case 'paste':
				// Allow
				break;

			case 'create':
				if (!strlen(Input::get('pid')) || !in_array(Input::get('pid'), $root))
				{
					$this->log('Not enough permissions to create location items in location archive ID "'.Input::get('pid').'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				break;

			case 'cut':
			case 'copy':
				if (!in_array(Input::get('pid'), $root))
				{
					$this->log('Not enough permissions to '.Input::get('act').' location item ID "'.$id.'" to location archive ID "'.Input::get('pid').'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				// NO BREAK STATEMENT HERE

			case 'edit':
			case 'show':
			case 'delete':
			case 'toggle':
			case 'feature':
				$objArchive = \Database::getInstance()->prepare("SELECT pid FROM tl_location WHERE id=?")
									->limit(1)
									->execute($id);

				if ($objArchive->numRows < 1)
				{
					$this->log('Invalid location item ID "'.$id.'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}

				if (!in_array($objArchive->pid, $root))
				{
					$this->log('Not enough permissions to '.Input::get('act').' location item ID "'.$id.'" of location archive ID "'.$objArchive->pid.'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				break;

			case 'select':
			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
			case 'cutAll':
			case 'copyAll':
				if (!in_array($id, $root))
				{
					$this->log('Not enough permissions to access location archive ID "'.$id.'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}

				$objArchive = \Database::getInstance()->prepare("SELECT id FROM tl_location WHERE pid=?")
									->execute($id);

				if ($objArchive->numRows < 1)
				{
					$this->log('Invalid location archive ID "'.$id.'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}

				$session = $this->Session->getData();
				$session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $objArchive->fetchEach('id'));
				$this->Session->setData($session);
				break;

			default:
				if (strlen(Input::get('act')))
				{
					$this->log('Invalid command "'.Input::get('act').'"', __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				elseif (!in_array($id, $root))
				{
					$this->log('Not enough permissions to access location archive ID ' . $id, __METHOD__, TL_ERROR);
					$this->redirect('contao?act=error');
				}
				break;
		}
	}


	public function strClass($varValue)
	{

		if (version_compare(VERSION . '.' . BUILD, '3.5.1', '<'))
		{
			$varValue = standardize(String::restoreBasicEntities($varValue));
		}
		else
		{
			$varValue = \StringUtil::generateAlias($varValue);
		}
		return $varValue;
	}


	/**
	 * Auto-generate the location alias if it has not been set yet
	 * @param mixed
	 * @param \DataContainer
	 * @return string
	 * @throws \Exception
	 */
	public function generateAlias($varValue, DataContainer $dc)
	{
		$autoAlias = false;

		// Generate alias if there is none
		if ($varValue == '')
		{
			$autoAlias = true;
			$varValue = $this->strClass($dc->activeRecord->title);
		}

		$objAlias = \Database::getInstance()->prepare("SELECT id FROM tl_location WHERE alias=?")
							->execute($varValue);

		// Check whether the location alias exists
		if ($objAlias->numRows > 1 && !$autoAlias)
		{
			throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
		}

		// Add ID to alias
		if ($objAlias->numRows && $autoAlias)
		{
			$varValue .= '-' . $dc->id;
		}

		return $varValue;
	}


	/**
	 * Add the type of input field
	 * @param array
	 * @return string
	 */
	public function listObjects($arrRow)
	{//print_r($arrRow);
		$key = $arrRow['published'] ? 'published' : 'unpublished';
		if($arrRow['date']) {
			$date = Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $arrRow['date']);
		}
		else {
			$date = Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $arrRow['tstamp']);
		}

		if($arrRow['addImage'] && $arrRow['singleSRC'])
		{
			if (!\Validator::isUuid($arrRow['singleSRC']))
			{
					return '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
			}
			$objFile = \FilesModel::findByUuid($arrRow['singleSRC']);
			//print_r($objImage);
			if($objFile) {
				$image = \Image::get($objFile->path, 75, 100, 'box');
				$image = '<img src="' . $image . '" alt="" />';
			}
		}

	return '
<div class="tr ' . $key . '" style="clear:both">
	<div class="td location_image">' . $image . '</div>
	<div class="td">
		<div class="location_title"><strong>' . $arrRow['title'] . '</strong></div>
		<div>' . $arrRow['postal'] . ' ' . $arrRow['city'] . '</div>
		<div>' . $arrRow['street'] . '</div>
	</div>
	<div class="td location_teaser">
		' . $arrRow['teaser'] . '
	</div>
</div>
' . "\n";
	}


	/**
	 * Return the link picker wizard
	 * @param \DataContainer
	 * @return string
	 */
	public function pagePicker(DataContainer $dc)
	{
		return ' <a href="contao/page.php?do='.Input::get('do').'&amp;table='.$dc->table.'&amp;field='.$dc->field.'&amp;value='.str_replace(array('{{link_url::', '}}'), '', $dc->value).'" onclick="Backend.getScrollOffset();Backend.openModalSelector({\'width\':765,\'title\':\''.specialchars(str_replace("'", "\\'", $GLOBALS['TL_LANG']['MOD']['page'][0])).'\',\'url\':this.href,\'id\':\''.$dc->field.'\',\'tag\':\'ctrl_'.$dc->field . ((Input::get('act') == 'editAll') ? '_' . $dc->id : '').'\',\'self\':this});return false">' . Image::getHtml('pickpage.svg', $GLOBALS['TL_LANG']['MSC']['pagepicker'], 'style="vertical-align:top;cursor:pointer"') . '</a>';
	}


	/**
	 * Return the "toggle visibility" button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		if (strlen(Input::get('tid')))
		{
			$this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
			$this->redirect($this->getReferer());
		}

		// Check permissions AFTER checking the tid, so hacking attempts are logged
		if (!$this->User->hasAccess('tl_location::published', 'alexf'))
		{
			return '';
		}

		$href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

		if (!$row['published'])
		{
			$icon = 'invisible.svg';
		}

		return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
	}


	/**
	 * Disable/enable a user group
	 *
	 * @param integer       $intId
	 * @param boolean       $blnVisible
	 * @param DataContainer $dc
	 */
	public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
	{
		// Check permissions to edit
		Input::setGet('id', $intId);
		Input::setGet('act', 'toggle');
		$this->checkPermission();

		// Check permissions to publish
		if (!$this->User->hasAccess('tl_location::published', 'alexf'))
		{
			$this->log('Not enough permissions to publish/unpublish location item ID "'.$intId.'"', __METHOD__, TL_ERROR);
			$this->redirect('contao?act=error');
		}

		$objVersions = new Versions('tl_location', $intId);
		$objVersions->initialize();

		// Trigger the save_callback
		if (is_array($GLOBALS['TL_DCA']['tl_location']['fields']['published']['save_callback']))
		{
			foreach ($GLOBALS['TL_DCA']['tl_location']['fields']['published']['save_callback'] as $callback)
			{
				if (is_array($callback))
				{
					$this->import($callback[0]);
					$blnVisible = $this->$callback[0]->$callback[1]($blnVisible, ($dc ?: $this));
				}
				elseif (is_callable($callback))
				{
					$blnVisible = $callback($blnVisible, ($dc ?: $this));
				}
			}
		}

		// Update the database
		\Database::getInstance()->prepare("UPDATE tl_location SET tstamp=". time() .", published='" . ($blnVisible ? 1 : '') . "' WHERE id=?")
				->execute($intId);

		$objVersions->create();
		$this->log('A new version of record "tl_location.id='.$intId.'" has been created'.$this->getParentEntries('tl_location', $intId), __METHOD__, TL_GENERAL);

	}
}