<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location_archive';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Name', 'Please enter the name of the group.');
$GLOBALS['TL_LANG'][$strTable]['jumpTo'] = array('Forward Page', 'Please select the detail page for the places are next sent to the visitors when you click on a Location.');
$GLOBALS['TL_LANG'][$strTable]['tstamp'] = array('Date Modified', 'The date and time of last modification');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['title_legend'] = 'Title';

/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['new'] = array('New group', 'A new group of event locations');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Details', 'Show the details of the group ID %s');
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Edit group', 'Edit the group ID %s');
$GLOBALS['TL_LANG'][$strTable]['editheader'] = array('Edit Settings', 'Edit the settings of the group ID %s');
$GLOBALS['TL_LANG'][$strTable]['copy'] = array('Duplicate Group', 'Duplicate the group ID %s');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Delete group', 'Delete the group ID %s');