<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Name', 'Please enter the name of the location.');
$GLOBALS['TL_LANG'][$strTable]['alias'] = array('Alias', 'Alias of the location');
$GLOBALS['TL_LANG'][$strTable]['tstamp'] = array('Date Modified', 'The date and time of last modification');

$GLOBALS['TL_LANG'][$strTable]['contact'] = array('Ansprechpartner', 'Sie können hier den Ansprechpartner für diesen Ort eingeben.');
$GLOBALS['TL_LANG'][$strTable]['company'] = array('Company', 'Here you can enter a company name.');
$GLOBALS['TL_LANG'][$strTable]['street'] = array('Street', 'Please enter the street name and house number.');
$GLOBALS['TL_LANG'][$strTable]['postal'] = array('Postal Code', 'Please enter the zip code.');
$GLOBALS['TL_LANG'][$strTable]['city'] = array('City', 'Please enter the name of the town or the village.');
$GLOBALS['TL_LANG'][$strTable]['state'] = array('State', 'Please enter the name of the state.');
$GLOBALS['TL_LANG'][$strTable]['country'] = array('Country', 'Please select a country.');

$GLOBALS['TL_LANG'][$strTable]['phone'] = array('Phone Number', 'Please enter the phone number.');
$GLOBALS['TL_LANG'][$strTable]['mobile'] = array('Mobile Phone Number', 'Please enter the mobile phone number.');
$GLOBALS['TL_LANG'][$strTable]['fax'] = array('Fax Number', 'Please enter the fax number.');
$GLOBALS['TL_LANG'][$strTable]['email'] = array('E-mail address', 'Please enter a valid email address.');
$GLOBALS['TL_LANG'][$strTable]['website'] = array('Website', 'Here you can enter a web address.');

$GLOBALS['TL_LANG'][$strTable]['published'] = array('Publish Location', 'Show the Location on the website.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['title_legend'] = 'Name and Alias';
$GLOBALS['TL_LANG'][$strTable]['address_legend'] = 'Address details';
$GLOBALS['TL_LANG'][$strTable]['contact_legend'] = 'Contact details';
$GLOBALS['TL_LANG'][$strTable]['image_legend'] = 'Teaser image';
$GLOBALS['TL_LANG'][$strTable]['publish_legend'] = 'Publication';


/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['new'] = array('New location', 'Einen neuen Ort anlegen');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Ortdetails', 'Details des Orts ID %s anzeigen');
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Ort bearbeiten', 'Ort ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['copy'] = array('Ort duplizieren', 'Ort ID %s duplizieren');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Ort löschen', 'Ort ID %s löschen');
$GLOBALS['TL_LANG'][$strTable]['toggle'] = array('Ort aktivieren/deaktivieren', 'Ort ID %s aktivieren/deaktivieren');