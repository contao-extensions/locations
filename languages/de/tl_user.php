<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_user';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['locations'] = array('Erlaubte Archive', 'Hier können Sie den Zugriff auf ein oder mehrere Adress-Archive erlauben.');
$GLOBALS['TL_LANG'][$strTable]['locationp'] = array('Archivrechte', 'Hier können Sie die Archivrechte festlegen.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['location_legend'] = 'Berechtigungen für Orte und Adressen';