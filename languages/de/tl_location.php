<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Name', 'Bitte geben Sie den Namen des Veranstaltungsortes ein.');
$GLOBALS['TL_LANG'][$strTable]['alias'] = array('Alias', 'Der Alias wird für die Erstellung des URL-Pfads benötigt.');
$GLOBALS['TL_LANG'][$strTable]['tstamp'] = array('Änderungsdatum', 'Datum und Uhrzeit der letzten Änderung');

$GLOBALS['TL_LANG'][$strTable]['contact'] = array('Ansprechpartner', 'Sie können hier den Ansprechpartner für diesen Ort eingeben.');
$GLOBALS['TL_LANG'][$strTable]['company'] = array('Firma', 'Hier können Sie einen Firmennamen eingeben.');
$GLOBALS['TL_LANG'][$strTable]['street'] = array('Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.');
$GLOBALS['TL_LANG'][$strTable]['postal'] = array('Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.');
$GLOBALS['TL_LANG'][$strTable]['city'] = array('Stadt', 'Bitte geben Sie den Namen der Stadt oder des Ortes ein.');
$GLOBALS['TL_LANG'][$strTable]['state'] = array('Staat', 'Bitte geben Sie den Namen des Staates ein.');
$GLOBALS['TL_LANG'][$strTable]['country'] = array('Land', 'Bitte wählen Sie ein Land.');

$GLOBALS['TL_LANG'][$strTable]['phone'] = array('Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.');
$GLOBALS['TL_LANG'][$strTable]['mobile'] = array('Handynummer', 'Bitte geben Sie die Handynummer ein.');
$GLOBALS['TL_LANG'][$strTable]['fax'] = array('Faxnummer', 'Bitte geben Sie die Faxnummer ein.');
$GLOBALS['TL_LANG'][$strTable]['email'] = array('E-Mail-Adresse', 'Bitte geben Sie eine gültige E-Mail-Adresse ein.');
$GLOBALS['TL_LANG'][$strTable]['website'] = array('Webseite', 'Hier können Sie eine Web-Adresse eingeben.');

$GLOBALS['TL_LANG'][$strTable]['published'] = array('Ort veröffentlichen', 'Den Ort auf der Webseite anzeigen.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['title_legend'] = 'Name und Alias';
$GLOBALS['TL_LANG'][$strTable]['image_legend']	= 'Teaserbild';
$GLOBALS['TL_LANG'][$strTable]['address_legend'] = 'Adressdaten';
$GLOBALS['TL_LANG'][$strTable]['contact_legend'] = 'Kontaktdaten';
$GLOBALS['TL_LANG'][$strTable]['publish_legend'] = 'Veröffentlichung';

/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['new'] = array('Neuer Ort', 'Einen neuen Ort anlegen');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Ortdetails', 'Details des Orts ID %s anzeigen');
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Ort bearbeiten', 'Ort ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['copy'] = array('Ort duplizieren', 'Ort ID %s duplizieren');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Ort löschen', 'Ort ID %s löschen');
$GLOBALS['TL_LANG'][$strTable]['toggle'] = array('Ort aktivieren/deaktivieren', 'Ort ID %s aktivieren/deaktivieren');