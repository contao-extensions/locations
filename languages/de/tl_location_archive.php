<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_location_archive';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['title'] = array('Name', 'Bitte geben Sie einen Namen für das Archiv ein.');

/**
 * Legends
 */
$GLOBALS['TL_LANG'][$strTable]['title_legend'] = 'Titel';

/**
 * Buttons
 */
$GLOBALS['TL_LANG'][$strTable]['new'] = array('Neues Archiv', 'Eine neue Archiv von Veranstaltungsorten anlegen');
$GLOBALS['TL_LANG'][$strTable]['show'] = array('Details', 'Die Details des Archiv ID %s anzeigen');
$GLOBALS['TL_LANG'][$strTable]['edit'] = array('Archiv bearbeiten', 'Das Archiv ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['editheader'] = array('Einstellungen', 'Die Einstellungen des Archiv ID %s bearbeiten');
$GLOBALS['TL_LANG'][$strTable]['copy'] = array('Archiv duplizieren', 'Das Archiv ID %s duplizieren');
$GLOBALS['TL_LANG'][$strTable]['delete'] = array('Archiv löschen', 'Das Archiv ID %s löschen');