<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */

$strTable = 'tl_news';

/**
 * Global operations
 */
$GLOBALS['TL_LANG'][$strTable]['locations'] = array('Orte', 'Orte hinzufügen oder bearbeiten.');

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['location'] = array('Ort', 'Sie können hier einen Ort eingeben auf den sich dieser Nachrichtenbeitrag bezieht.');