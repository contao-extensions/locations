<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2023 Leo Feyer
 *
 * @package   contao-locations
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2015-2023 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Locations;


class ModuleNewsChange extends \Frontend
{
	/**
	 * Template
	 * @var string
	 */

	public function modifyTemplate($objTemplate)
	{
		if (strpos($objTemplate->getName(), 'news_') === 0 && $objTemplate->location)
		{
			if(\Config::get('location_templates'))
			{
				return '';
			}

			global $objPage;

			//Get Location
			$objLocation = \LocationModel::findPublishedByIdOrAlias($objTemplate->location);

			if($objLocation->id)
			{
				$objLocation->id = $objTemplate->location;
			}

			$objTemplate->location = $objLocation;
		}
	}
}
